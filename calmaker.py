
import datetime
from datetime import date
import calendar
from calendar import monthrange
import sys


class CalTools:
    def __init__(self,first_weekday_of_week,abbrev_months_bool):
        self.weekday_start_offset = (first_weekday_of_week - 1) % 7
        self.abbrev_months_bool = abbrev_months_bool

    """
    numDaysInMonth(self,year,month)
    ==========
    Returns 31 for January, etc.
    """
    def numDaysInMonth(self,year,month):
        return monthrange(year, month)[1]

    """
    numDaysInYear(self,year)
    ==========
    Returns 365 for 2013, 366 for 2012
    """
    def numDaysInYear(self,year):
        count = 0
        for i in range(1,12+1):
            count += self.numDaysInMonth(year,i)
        return count

    """
    dayOfWeek(self,year,month,day)
    ==========
    Returns 1 = Monday, 2 = Tuesday, etc.
    Unless 'weekday_start_offset' is non-zero, which causes offset.
    """
    def dayOfWeek(self,year,month,day):
        return ((date(year,month,day).weekday())-self.weekday_start_offset)%7 + 1

    """
    weekdayName(self,weekday_int)
    ========
    Returns "Monday" for 1, etc., but offset by the offset parameter.
    """
    def weekdayName(self,weekday_int):
        return calendar.day_name[(weekday_int-1+self.weekday_start_offset)%7]


    """
    monthName(self,month_int)
    ========
    Returns "Jan" for 1, etc.
    """
    def monthName(self,month_int):
        if(self.abbrev_months_bool):
            months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
        else:
            months = ["January","February",
                      "March","April",
                      "May","June",
                      "July","August",
                      "September","October",
                      "November","December"]
        return months[month_int-1]


    """
    weekIndexForDayOfYear(self,year,day_of_year)
    =========
    Returns the week index for a yearday (1...366, but still works with out-of-range input)
    """
    def weekIndexForDayOfYear(self,year,day_of_year):
        # Find the weekday index of the day in the year (one-based index)
        jan1_weekday = self.dayOfWeek(year,1,1)
        yearday_of_week_2_weekday_1 = 7-jan1_weekday+2
        yearday_of_week_1_weekday_1 = yearday_of_week_2_weekday_1-7
        weekindex = (day_of_year - yearday_of_week_1_weekday_1) // 7 + 1
        return weekindex

    """
    YearMonthDayForYearWeekIndexWeekDay(self,year,weekindex,weekday)
    ========
    year = the year (example: 2016)
    weekindex = 1....54
    weekday = 1...7
    Returns Year,Month,Day for the given week index.
    (Year may be the year before or year after, if running over end of year)
    """
    def YearMonthDayForYearWeekIndexWeekDay(self,year,weekindex,weekday):
        # Find the index of the day in the year (one-based index)
        jan1_weekday = self.dayOfWeek(year,1,1)
        day_index_in_year = (weekindex-1)*7 + weekday - jan1_weekday + 1
        # Cases before or beyond the year bounds:
        if( day_index_in_year < 1 ):
            # Figure out which week of the previous year we're in.
            num_days_prevyear = self.numDaysInYear(year-1)
            yearday_prevyear = num_days_prevyear + day_index_in_year
            weekindex_prevyear = self.weekIndexForDayOfYear(year-1,yearday_prevyear)
            return self.YearMonthDayForYearWeekIndexWeekDay(year-1,weekindex_prevyear,weekday)
        if( day_index_in_year > self.numDaysInYear(year) ):
            yearday_nextyear = day_index_in_year - self.numDaysInYear(year)
            weekindex_nextyear = self.weekIndexForDayOfYear(year+1,yearday_nextyear)
            return self.YearMonthDayForYearWeekIndexWeekDay(year+1,weekindex_nextyear,weekday)
        # Cases within the year:
        # Find the month we're in
        m = 1
        d = self.numDaysInMonth(year,m)
        prev_months_days = 0
        while( day_index_in_year > d ):
            prev_months_days += self.numDaysInMonth(year,m)
            m = m + 1
            d = d + self.numDaysInMonth(year,m)

        return year,m,day_index_in_year-prev_months_days

"""
class CalDay
==========
Each calendar day knows its position on the page, its date, and how
to draw itself into the postscript file.
"""
class CalDay:
    def __init__(self,year,month,day,weekindex,weekday,width,height,X,caltools,calparent):
        self.year = year
        self.month = month
        self.day = day
        self.weekindex = weekindex
        self.weekday = weekday
        self.width = width
        self.height = height
        self.X = X
        self.caltools = caltools
        self.calparent = calparent
        self.prev_day = None
        self.next_day = None
        self.prev_week = None
        self.next_week = None
        self.first_month_in_cal = False
    def draw(self,f):
        # Draw the box
        day_xmin = self.X[0]
        day_ymin = self.X[1]
        day_xmax = day_xmin + self.width
        day_ymax = day_ymin + self.height
        f.write("\n")
        f.write("0 0 0 setrgbcolor % black\n")
        f.write("2 setlinewidth\n")
        f.write("newpath\n")
        f.write(str(day_xmin)+" "+str(day_ymin)+" moveto\n")
        f.write(str(day_xmax)+" "+str(day_ymin)+" lineto\n")
        f.write(str(day_xmax)+" "+str(day_ymax)+" lineto\n")
        f.write(str(day_xmin)+" "+str(day_ymax)+" lineto\n")
        f.write(str(day_xmin)+" "+str(day_ymin)+" lineto\n")
        f.write("stroke\n")
        # Draw thick lines to separate months
        if((self.prev_day != None and self.prev_day.month != self.month)
                or (self.weekday == 1)):
            # draw left
            f.write("0 0 0 setrgbcolor % black\n")
            f.write("6 setlinewidth\n")
            f.write("newpath\n")
            f.write(str(day_xmin)+" "+str(day_ymin)+" moveto\n")
            f.write(str(day_xmin)+" "+str(day_ymax)+" lineto\n")
            f.write("stroke\n")
        if((self.next_day != None and self.next_day.month != self.month)
                or (self.weekday == 7)):
            # draw right
            f.write("0 0 0 setrgbcolor % black\n")
            f.write("6 setlinewidth\n")
            f.write("newpath\n")
            f.write(str(day_xmax)+" "+str(day_ymin)+" moveto\n")
            f.write(str(day_xmax)+" "+str(day_ymax)+" lineto\n")
            f.write("stroke\n")
        if((self.prev_week != None and self.prev_week.month != self.month)
                or (self.weekindex == self.calparent.cal_week_start and self.day <= 7)):
            # draw top
            f.write("0 0 0 setrgbcolor % black\n")
            f.write("6 setlinewidth\n")
            f.write("newpath\n")
            f.write(str(day_xmin)+" "+str(day_ymax)+" moveto\n")
            f.write(str(day_xmax)+" "+str(day_ymax)+" lineto\n")
            f.write("stroke\n")
        if((self.next_week != None and self.next_week.month != self.month)
                or (self.weekindex == self.calparent.cal_week_end and self.day >= 15)):
            # draw bottom
            f.write("0 0 0 setrgbcolor % black\n")
            f.write("6 setlinewidth\n")
            f.write("newpath\n")
            f.write(str(day_xmin)+" "+str(day_ymin)+" moveto\n")
            f.write(str(day_xmax)+" "+str(day_ymin)+" lineto\n")
            f.write("stroke\n")
        # Draw the day number
        f.write("/Times-Roman findfont "+str(self.calparent.day_fontsize)+" scalefont setfont % Declare the current font\n")
        f.write("0 0 0 setrgbcolor\n")
        text_string = str(self.day)
        daynum_text_x = self.X[0]+0.33*self.calparent.day_fontsize
        daynum_text_y = self.X[1]+self.height-self.calparent.day_fontsize
        f.write(str(daynum_text_x)+"   "+str(daynum_text_y)+"   moveto\n")
        f.write("("+text_string+") show\n")
        f.write("\n")
        # If we're the first day of the month on the left side, then we draw the month title.
        if( (self.prev_week != None and self.prev_week.month != self.month and self.weekday == 1)
                or (self.prev_week == None and self.weekday == 1 and self.day==1)):
            f.write("% x y fontsize rotation (text) outputtext\n")
            text_x = self.X[0] - self.calparent.below_month_buffer_fraction*self.calparent.month_fontsize
            text_y = self.X[1]
            text_string = self.caltools.monthName(self.month)
            #if(self.next_week != None and self.first_month_in_cal == True):
            #    text_string += ", " + str(self.year)
            f.write(str(text_x)+" "+str(text_y)+" "+str(self.calparent.month_fontsize)+" "+"90 ("+text_string+") outputtext\n")
            if(self.next_week != None and (self.first_month_in_cal == True or self.month == 1)):
                self.next_week.writeYear(f)
        # Draw the hours in the day
        if(self.calparent.show_hrs):
            hour_fontsize = (daynum_text_y - (self.X[1]+5)) / 24
            for h_i in range(0,24):
                hour = (h_i-1)%12 + 1
                f.write("/Times-Roman findfont "+str(hour_fontsize)+" scalefont setfont % Declare the current font\n")
                f.write("0 0 0 setrgbcolor\n")
                text_string = str(hour)#+":00"
                if(hour % 6 == 0):
                    if( h_i <= 11 ):
                        text_string += " am"
                    else:
                        text_string += " pm"
                text_x = self.X[0]+self.width/16
                text_y = daynum_text_y - (h_i+1)*hour_fontsize
                f.write(str(text_x)+"   "+str(text_y)+"   moveto\n")
                f.write("("+text_string+") show\n")
                f.write("\n")
        # Draw the title of the day of the week.
        if(self.weekindex == self.calparent.cal_week_start):
            f.write("/Times-Roman findfont "+str(self.calparent.weekday_fontsize)+" scalefont setfont % Declare the current font\n")
            f.write("0 0 0 setrgbcolor\n")
            text_string = self.caltools.weekdayName(self.weekday)
            text_x = self.X[0]+self.width/5
            text_y = self.X[1]+self.height + self.calparent.below_weekday_buffer_fraction*self.calparent.weekday_fontsize
            f.write(str(text_x)+"   "+str(text_y)+"   moveto\n")
            f.write("("+text_string+") show\n")
            f.write("\n")
        return

    def writeYear(self,f):
        f.write("% x y fontsize rotation (text) outputtext\n")
        text_x = self.X[0] - self.calparent.below_month_buffer_fraction*self.calparent.month_fontsize
        text_y = self.X[1]
        text_string = str(self.year)
        f.write(str(text_x)+" "+str(text_y)+" "+str(self.calparent.month_fontsize)+" "+"90 ("+text_string+") outputtext\n")

class Cal:
    def __init__(self,
                 PARAM_cal_year,
                 PARAM_cal_week_start,
                 PARAM_cal_week_end,
                 PARAM_first_weekday_of_week,
                 PARAM_page_width,
                 PARAM_page_height,
                 PARAM_margin,
                 PARAM_title,
                 PARAM_title_fontsize,
                 PARAM_month_fontsize,
                 PARAM_weekday_fontsize,
                 PARAM_day_fontsize,
                 PARAM_abbrev_months,
                 PARAM_show_hrs,
                 PARAM_output_filename):

        # Global variables
        self.first_weekday_of_week = PARAM_first_weekday_of_week
        self.page_width = 72*PARAM_page_width
        self.page_height = 72*PARAM_page_height
        self.page_margin = 72*PARAM_margin
        self.cal_year = PARAM_cal_year
        self.cal_week_start = PARAM_cal_week_start
        self.cal_week_end = PARAM_cal_week_end
        self.title = PARAM_title
        self.title_fontsize = PARAM_title_fontsize
        self.below_title_buffer_fraction = 0.5
        self.below_month_buffer_fraction = 0.35
        self.below_weekday_buffer_fraction = 0.35
        self.ave_char_width_fraction = 0.20
        self.month_fontsize = PARAM_month_fontsize
        self.weekday_fontsize = PARAM_weekday_fontsize
        self.day_fontsize = PARAM_day_fontsize
        self.abbrev_months_bool = PARAM_abbrev_months
        self.show_hrs = PARAM_show_hrs
        self.output_filename = PARAM_output_filename

        #Total page
        self.page_xmin = 0
        self.page_ymin = 0
        self.page_xmax = self.page_xmin + self.page_width
        self.page_ymax = self.page_ymin + self.page_height

        #Drawable page
        self.dpage_xmin = self.page_margin + self.month_fontsize
        self.dpage_ymin = self.page_margin
        self.dpage_xmax = self.page_xmin + self.page_width - self.page_margin
        self.dpage_ymax = self.page_ymin + self.page_height - self.page_margin - \
                self.title_fontsize - self.below_title_buffer_fraction*self.title_fontsize - self.weekday_fontsize - \
                self.below_weekday_buffer_fraction*self.weekday_fontsize
        self.title_y = self.page_ymin + self.page_height - self.page_margin - \
                self.title_fontsize

        self.caltools = CalTools(self.first_weekday_of_week,self.abbrev_months_bool)

        # Create an array of the days in the year
        self.days_in_cal = []
        for wi in range(self.cal_week_start,self.cal_week_end+1):
            for wd in range(1,7+1):
                y,m,d = self.caltools.YearMonthDayForYearWeekIndexWeekDay(self.cal_year,wi,wd)
                wid = (self.dpage_xmax-self.dpage_xmin)/7
                hei = (self.dpage_ymax-self.dpage_ymin)/(self.cal_week_end-self.cal_week_start+1)
                week_loop_i = wi - self.cal_week_start + 1
                week_loops_count = self.cal_week_end - self.cal_week_start + 1
                day_X = [self.dpage_xmin+(wd-1)*wid, self.dpage_ymin+(week_loops_count-week_loop_i)*hei]
                self.days_in_cal.append(CalDay(y,m,d,wi,wd,wid,hei,day_X,self.caltools,self))

        # Connect each day to their neighbors
        for i in range(0,len(self.days_in_cal)):
            if(i > 0):
                self.days_in_cal[i].prev_day = self.days_in_cal[i-1]
            if(i < len(self.days_in_cal)-1):
                self.days_in_cal[i].next_day = self.days_in_cal[i+1]
            if(i >= 7):
                self.days_in_cal[i].prev_week = self.days_in_cal[i-7]
            if(i < len(self.days_in_cal)-7):
                self.days_in_cal[i].next_week = self.days_in_cal[i+7]

        # Mark the first full month of the calendar
        bool_in_first_month = False
        for i in range(0,len(self.days_in_cal)):
            if( bool_in_first_month == False and self.days_in_cal[i].day == 1 ):
                bool_in_first_month = True
            elif( bool_in_first_month == True and self.days_in_cal[i].day == 1):
                bool_in_first_month = False
                break
            if( bool_in_first_month ):
                self.days_in_cal[i].first_month_in_cal = True
                #print(bool_in_first_month,self.days_in_cal[i].year,self.days_in_cal[i].month,self.days_in_cal[i].day)


    def draw(self):
        f = open(self.output_filename, "w")
        self.drawHeader(f)
        for cal_day in self.days_in_cal:
            cal_day.draw(f)
        f.close()

    def drawHeader(self,f):
        f.write("%!PS-Adobe-2.0 EPSF-1.2\n")
        f.write("%%BoundingBox: "+str(self.page_xmin)+" "+str(self.page_ymin)+" "+str(self.page_xmax)+" "+str(self.page_ymax)+"\n")
        f.write("/Times-Roman findfont "+str(self.title_fontsize)+" scalefont setfont % Declare the current font\n")
        f.write("0 0 0 setrgbcolor\n")
        if(self.title != ""):
            text_string = self.title
        else:
            text_string = str(self.cal_year)+" Calendar"
        total_page_height = self.page_ymax-self.page_ymin
        text_x = self.page_width/2 - len(text_string)*self.ave_char_width_fraction*self.title_fontsize
        text_y = self.title_y
        f.write(str(text_x)+"   "+str(text_y)+"   moveto\n")
        f.write("("+text_string+") show\n")
        f.write("% Rotate text function thanks to http://stackoverflow.com/questions/2459459/rotating-text-in-postscript\n")
        f.write("/outputtext {\n")
        f.write("    /data exch def\n")
        f.write("    /rot exch def\n")
        f.write("    /xfont exch def\n")
        f.write("    /y1 exch def\n")
        f.write("    /x1 exch def\n")
        f.write("    /Times-Roman findfont\n")
        f.write("    xfont scalefont\n")
        f.write("    setfont\n")
        f.write("    x1 y1 moveto\n")
        f.write("    rot rotate\n")
        f.write("    data show\n")
        f.write("    rot neg rotate\n")
        f.write("} def\n")
        f.write("\n")



def main():
    print("calmaker.")
    #print(sys.argv)

    if(len(sys.argv) < 16):
        print("")
        print("Invalid Arguments!")
        print("")
        print("Usage:")
        print("    python3 calmaker.py <y> <w_s> <w_e> <wd_f> <p_w> <p_h> <p_m> <titl> <t_fs> <m_fs> <wd_fs> <d_fs> <ab_m> <hrs> <out>")
        print("")
        print("y: year")
        print("w_s: Week start, e.g. 1 = first week of the year")
        print("w_e: Week end, e.g. 2 = second week of the year")
        print("wd_f: First weekday of the week, e.g. 1 = week starts with Monday, 2 = week starts with Tuesday.")
        print("p_w: Page width, in inches")
        print("p_h: Page height, in inches")
        print("p_m: Page margin, in inches")
        print("titl: The title of the calendar (in quotation marks, \"Like This\")")
        print("t_fs: Title font size")
        print("m_fs: Month font size (Font size of month labels)")
        print("wd_fs: Weekday font size (Font size of weekday name labels)")
        print("d_fs: Day font size (Font size of day labels)")
        print("ab_m: Abbreviated month labels? 0 = No, 1 = Yes")
        print("hrs: Show hours of the day? 0 = No, 1 = Yes")
        print("out: Output file name, e.g. \"calendar.eps\"")
        print("")
        print("Example:   python3 calmaker.py 2017 -1 18 0 12 60 0.5 \"2017 Winter\" 48 48 12 24 0 1 output.eps")
        print("")
        exit()

    # Parameters (To be passed in via command line)
    PARAM_cal_year = int(sys.argv[1]) # e.g. 2016
    PARAM_cal_week_start = int(sys.argv[2]) # index of week in the year
    PARAM_cal_week_end = int(sys.argv[3]) # index of week in the year
    PARAM_first_weekday_of_week = int(sys.argv[4]) # shifting the first day of the week
    PARAM_page_width = float(sys.argv[5]) #inches
    PARAM_page_height = float(sys.argv[6])  #inches
    PARAM_margin = float(sys.argv[7]) #inches
    PARAM_title = sys.argv[8] # string
    PARAM_title_fontsize = int(sys.argv[9]) #point (font size unit)
    PARAM_month_fontsize = int(sys.argv[10]) #point (font size unit)
    PARAM_weekday_fontsize = int(sys.argv[11]) #point (font size unit)
    PARAM_day_fontsize = int(sys.argv[12]) #point (font size unit)
    PARAM_abbrev_months = int(sys.argv[13]) # 0 = false, 1 = true
    PARAM_show_hrs = int(sys.argv[14]) # 0 = false, 1 = true
    PARAM_output_filename = sys.argv[15] # string

    # create the calendar
    calendar = Cal(PARAM_cal_year,
                   PARAM_cal_week_start,
                   PARAM_cal_week_end,
                   PARAM_first_weekday_of_week,
                   PARAM_page_width,
                   PARAM_page_height,
                   PARAM_margin,
                   PARAM_title,
                   PARAM_title_fontsize,
                   PARAM_month_fontsize,
                   PARAM_weekday_fontsize,
                   PARAM_day_fontsize,
                   PARAM_abbrev_months,
                   PARAM_show_hrs,
                   PARAM_output_filename )
    calendar.draw()
    print("Output file: ",PARAM_output_filename)


if __name__ == "__main__":
    main()
