
-----------
calmaker.py
-----------

* Usage:  python3 calmaker.py <y> <w_s> <w_e> <wd_f> <p_w> <p_h> <p_m> <titl> <t_fs> <m_fs> <wd_fs> <d_fs> <ab_m> <hrs> <out>
* y: year
* w_s: Week start, e.g. 1 = first week of the year
* w_e: Week end, e.g. 2 = second week of the year
* wd_f: First weekday of the week, e.g. 1 = week starts with Monday, 2 = week starts with Tuesday.
* p_w: Page width, in inches
* p_h: Page height, in inches
* p_m: Page margin, in inches
* titl: The title of the calendar (in quotation marks, "Like This")
* t_fs: Title font size
* m_fs: Month font size (Font size of month labels)
* wd_fs: Weekday font size (Font size of weekday name labels)
* d_fs: Day font size (Font size of day labels)
* ab_m: Abbreviated month labels? 0 = No, 1 = Yes
* hrs: Show hours of the day? 0 = No, 1 = Yes
* out: Output file name, e.g. "calendar.eps"

--------
Example
--------

python3 calmaker.py 2017 -1 18 0 12 60 0.5 "2017 Winter" 48 48 12 24 0 1 output.eps
